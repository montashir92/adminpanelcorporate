<?php

use App\Models\WelComeNote;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWelComeNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wel_come_notes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('save_by', 3)->nullable();
            $table->string('update_by', 3)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        $welcome =new WelComeNote();
        $welcome->title = 'welcome title';
        $welcome->description = 'Description here';
        $welcome->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wel_come_notes');
    }
}
