<?php

use App\Models\Message;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('sender_name', 50);
            $table->string('phone', 15)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('subject', 150)->nullable();
            $table->text('message')->nullable();
            $table->string('ip_address', 15)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        $message = new Message();
        $message->sender_name = 'Md. Shamim Miah';
        $message->phone = '0170000000';
        $message->email = 'sample@gmail.com';
        $message->subject = 'test message subject';
        $message->message = 'Test message here';
        $message->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
