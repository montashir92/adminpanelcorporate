<?php

use GuzzleHttp\Middleware;
use Brian2694\Toastr\Toastr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\ContentController;
use App\Http\Controllers\Admin\MessageController;
use App\Http\Controllers\Admin\PhotoGalleryController;
use App\Http\Controllers\Admin\PortfolioController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VideoGalleryController;
use App\Http\Controllers\Admin\WelcomenoteController;

// use GuzzleHttp\Middleware;



// Route::get('/', function () {
//     return view('welcome');
// });
// optimiZe
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return 'DONE'; //Return anything
});

// admin route
// admin route
Route::get('/login',[AuthController::class, 'loginShow'])->name('login');
Route::post('/login',[AuthController::class, 'authCheck'])->name('login.check');

Route::group(['middleware' => ['auth']] , function(){
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/',function(){
        return view('admin.index');
    })->name('dashboard');

    // admin all route here
    Route::prefix('setting')->group(function(){
             // Admin Register
            Route::get('/user-edit', [UserController::class, 'edit'])->name('user.edit');
            Route::put('/user-update', [UserController::class, 'updateUser'])->name('user.update');
            Route::get('/password/change', [UserController::class, 'passwordChange'])->name('password.change');
            Route::post('/password/update', [UserController::class, 'passwordUpdate'])->name('password.update');
    });

    //welcome note route
    Route::get('/welcome-note',[WelcomenoteController::class,'edit'])->name('welcome-note.edit');
    Route::put('/welcome-note/update',[WelcomenoteController::class,'update'])->name('welcome-note.update');

    //slider  route
    Route::resource('/slider',SliderController::class)->except('show','create');

    //service  route
    Route::resource('/service',ServiceController::class)->except('show','create');

   
    Route::prefix('product')->group(function(){
         //category  route
        Route::resource('/category',CategoryController::class)->except('show','create');
        //product  route
        Route::resource('/product',ProductController::class)->except('show');
    });
    

    //clident  route
    Route::resource('/client',ClientController::class)->except('show','create');

    //photo-gallery  route
    Route::resource('/photo-gallery',PhotoGalleryController::class)->except('show','create');

    //video-gallery  route
    Route::resource('/video-gallery',VideoGalleryController::class)->except('show','create');

      //Contact us  route
    Route::get('/contact-us',[ContactUsController::class,'index'])->name('contact.us');
    Route::post('/contact-us/update',[ContactUsController::class,'update'])->name('contact-us.update');
   
    //Content  route
    Route::get('content',[ContentController::class,'edit'])->name('content');
    Route::post('content-update',[ContentController::class,'update'])->name('content.update');
    Route::get('content-active',[ContentController::class,'active'])->name('content.active');

    //Portfolio  route
    Route::get('profile',[ProfileController::class,'edit'])->name('profile');
    Route::post('profile-update',[ProfileController::class,'update'])->name('profile.update');

     //message  route
     Route::get('message',[MessageController::class,'index'])->name('message');
     Route::delete('message-delete/{id}',[MessageController::class,'destroy'])->name('message.delete');

});

   

