<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->get();
        return view('admin.client.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required|max:200',
            'image' => 'required|max:10000||Image|mimes:jpg,png,jpeg,bmp',
          ]);

          $slider = new Client();
          $slider->name = $request->name;
          $slider->image = $this->imageUpload($request, 'image', 'uploads/client');
          $slider->save();

          if($slider){
            return back()->with('success','Slider Added Successfully');
          }
          else{
            return back()->with('error','Slider Added Fail!');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.client.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'name'=> 'max:255',
            'image' => 'max:1000||Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $client = Client::first();
        $clientImage = '';
        if ($request->hasFile('image')) {
            if (file_exists($client->image)) {
                 unlink($client->image);
            }
            $clientImage = $this->imageUpload($request, 'image', 'uploads/client');
        } else {
            $clientImage = $client->image;
        }

        $client->name = $request->name;
        $client->image = $clientImage;
        $client->save();

        return redirect()->route('client.index')->with('success','Client Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::where('id', $id)->first();
        if ($client->image) {
            @unlink($client->image);
        }
        $client->delete();
        return back()->with('success','Client Deleted Successfully');
    }
}
