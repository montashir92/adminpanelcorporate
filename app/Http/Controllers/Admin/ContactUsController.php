<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index(){
        $contact = ContactUs::first();
        return view('admin.contact.index',compact('contact'));
    }
    public function update(Request $request){
       $contact = ContactUs::first();
       $contact->slogan = $request->slogan;
       $contact->phone = $request->phone;
       $contact->email = $request->email;
       $contact->facebook = $request->facebook;
       $contact->twitter = $request->twitter;
       $contact->linkedin = $request->linkedin;
       $contact->youtube = $request->youtube;
       $contact->instagram = $request->instagram;
       $contact->map = $request->map;
       $contact->save();
       return back()->with('success','Contac Us Updated Successfully');
    }
}

