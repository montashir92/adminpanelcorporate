<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\WelComeNote;
use Illuminate\Http\Request;

class WelcomenoteController extends Controller
{
    public function edit()
    {
        $welcome = WelComeNote::first();
        return view('admin.welcome.edit', compact('welcome'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'max:255',
            'description' => 'string',
            'image' => 'image|mimes:jpg,png,gif,bmp',
        ]);

        $welcome = WelComeNote::first();

        $welcomeImage = '';
    //    return $request->hasFile('image');
        if ($request->hasFile('image')) {
            if (!empty($welcome->image) && file_exists($welcome->image)) {
                unlink($welcome->image);
                $welcomeImage = $this->imageUpload($request, 'image', 'uploads/welcome');
            } else {
                $welcomeImage = $welcome->image;
            }
            $welcomeImage = $this->imageUpload($request, 'image', 'uploads/welcome');
        }

        $welcome->title = $request->title;
        $welcome->description = $request->description;
       $welcome->image =  $welcomeImage;
        $welcome->save();
        return back()->with('success', 'Welcome Note Updated Successfully');
    }
}
