<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|max:200',
            'image' => 'required|max:10000||Image|mimes:jpg,png,jpeg,bmp',
          ]);

          $slider = new Slider();
          $slider->title = $request->title;
          $slider->image = $this->imageUpload($request, 'image', 'uploads/slider');
          $slider->save();

          if($slider){
            return back()->with('success','Slider Added Successfully');
          }
          else{
            return back()->with('error','Slider Added Fail!');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'title'=> 'max:255',
            'image' => 'max:1000||Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $slider = Slider::first();
        $sliderImage = '';
        if ($request->hasFile('image')) {
            if (file_exists($slider->image)) {
                 unlink($slider->image);
            }
            $sliderImage = $this->imageUpload($request, 'image', 'uploads/slider');
        } else {
            $sliderImage = $slider->image;
        }

        $slider->title = $request->title;
        $slider->image = $sliderImage;
        $slider->save();

        return redirect()->route('slider.index')->with('success','Slider Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::where('id', $id)->first();
        if ($slider->image) {
            @unlink($slider->image);
        }
        $slider->delete();
        return back()->with('success','Slider Deleted Successfully');
    }
}
