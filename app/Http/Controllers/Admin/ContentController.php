<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function edit(){
        $content = Content::first();
        return view('admin.content.edit',compact('content'));
    }

    public function update(Request $request){
        $request->validate([
            'title' => 'max:255',
            'description' => 'string',
            'image' => 'max:1000||Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $content = Content::first();
        $contentImage = '';
        if ($request->hasFile('image')) {
            if (file_exists($content->image)) {
                 unlink($content->image);
            }
            $contentImage = $this->imageUpload($request, 'image', 'uploads/slider');
        } else {
            $contentImage = $content->image;
        }

        $content->title = $request->title;
        $content->description = $request->description;
        $content->image = $contentImage;
        $content->save();

        return back()->with('success','Content Updated Successfully');
    }

    public function active(){
        $content = Content::first();
        if($content->status == true){
            $content->status = false;
            $content->save();
            return back()->with('success','Content Deactive Successfully');
        }
        else{
            $content->status = true;
            $content->save();
            return back()->with('success','Content Active Successfully');
        }
    }
}
