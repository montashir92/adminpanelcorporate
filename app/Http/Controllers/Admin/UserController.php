<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function edit()
    {
        $user = User::where('id',Auth::user()->id)->first();
        return view('auth.edit', compact('user'));
    }

    public function updateUser(Request $request)
    {

        $request->validate([
            'name' => 'required|max:50',
            // 'username' =>'required',
            'email' => 'required|max:30',
            'image' => 'image|mimes:jpg,png,gif,bmp',
            // 'password'=>'confirmed|min:2',
            'ip_address' => 'max:15'
        ]);
        $user = User::where('id',Auth::user()->id)->first();
      

            $userImage = '';
            if ($request->hasFile('image')) {
                if (!empty($user->image) && file_exists($user->image)) {
                    unlink($user->image);
                }
                $userImage = $this->imageUpload($request, 'image', 'uploads/user');
            } else {
                $userImage = $user->image;
            }
            $user->name = $request->name;
            $user->email = $request->email;
            $user->image = $userImage;
            $user->status = 1;
            $user->updated_by = Auth::user()->id;
            $user->ip_address = $request->ip();
            $user->save();
            if ($user) {
                Session::flash('success', ' User update Successfully');
                return back();
            } else {
                Session::flash('errors', ' something went wrong');
            }
        
    }

    public function passwordChange(){
       return view('auth.password_change');
    }
    public function passwordUpdate(Request $request){
        $request->validate([
            'currentPass' => 'required',
            'password' => 'required|confirmed|min:2',
        ]);
        $currentPassword = Auth::user()->password;
        if (Hash::check($request->currentPass, $currentPassword)) {
            if (!Hash::check($request->password, $currentPassword)) {
                $user =Auth::user();
                $user->password = HasH::make($request->password);
                $user->save();
                if ($user) {
                    Session::flash('success', 'Password Update Successfully');
                   
                    return back();
                } else {
                    Session::flash('error', 'Current password not match');
                    return back();
                }
            } else {
                Session::flash('error', 'Same as Current password');
                return back();
            }
        } else {
            Session::flash('error', '!Current password not match');
            return back();
        }
    }
   
    
}
