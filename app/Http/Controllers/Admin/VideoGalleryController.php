<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\VideoGallery;
use Illuminate\Http\Request;

class VideoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videoGallery = VideoGallery::latest()->get();
        return view('admin.videoGallery.index',compact('videoGallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|max:200',
            'video_link'=> 'required|max:255',
          ]);

          $videoGallery = new VideoGallery();
          $videoGallery->title = $request->title;
          $videoGallery->video_link = $request->video_link;
          $videoGallery->ip_address = $request->ip();
          $videoGallery->save();

          if($videoGallery){
            return back()->with('success','videoGallery Added Successfully');
          }
          else{
            return back()->with('error','videoGallery Added Fail!');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoGallery $videoGallery)
    {
        return view('admin.videoGallery.edit',compact('videoGallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'video_link'=> 'max:255',
            'image' => 'string',
        ]);

        $client = VideoGallery::where('id',$id)->first();
        $client->video_link = $request->video_link;
        $client->title = $request->title;
        $client->save();

        return redirect()->route('video-gallery.index')->with('success','videoGallery Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $videoGallery = VideoGallery::where('id', $id)->first();
        $videoGallery->delete();
        return back()->with('success','videoGallery Deleted Successfully');
    }
}
