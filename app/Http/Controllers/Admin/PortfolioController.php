<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Models\Portfolioa;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function edit(){
        $portfolio = Portfolio::first();
        return view('admin.profile.edit',compact('portfolio'));
    }

    public function update(Request $request){
        $request->validate([
            'title' => 'max:255',
            'description' => 'string',
            'image' => 'max:1000|Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $portfolio = Portfolio::first();
        $portfolioImage = '';
        if ($request->hasFile('image')) {
            if (!empty($portfolio->image) && file_exists($portfolio->image)) {
                unlink($portfolio->image);
                $portfolioImage = $this->imageUpload($request, 'image', 'uploads/portfolio');
            } else {
                $portfolioImage = $portfolio->image;
            }
            $portfolioImage = $this->imageUpload($request, 'image', 'uploads/portfolio');
        }

        $portfolio->title = $request->title;
        $portfolio->description = $request->description;
        $portfolio->image = $portfolioImage;
        $portfolio->save();

        return back()->with('success','Portfolio Updated Successfully');
    }
}
