<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $category = Category::all();
        $product = Product::latest()->get();
        return view('admin.product.index', compact('product','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required|max:100',
            'category_id' => 'required',
            'price' => 'required|max:10|regex:/^\d+(\.\d{1,2})?$/',
            'image' => 'required|image|mimes:jpg,png,gif,bmp|max:500',
        ]);
        $product = new Product();
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->image = $this->imageUpload($request, 'image', 'uploads/product');
        $product->description = $request->description;
        $product->save_by = 1;
        $product->ip_address = $request->ip();
        $product->save();
        return back()->with('success','Product Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $product = Product::where('id', $id)->first();
        return view('admin.product.edit', compact('product', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:100',
            'category_id' => 'required',
            'price' => 'required|max:18',
            'image' => 'image|mimes:jpg,png,gif,bmp|max:500',
            'ip_address' => 'max:15',
            // 'code' => 'max:18|unique:products,id',
        ]);

            $product = Product::find($id);
            
           
                $product_image = '';
                $product = Product::where('id', $id)->first();
                if ($request->hasFile('image')) {
                    if (!empty($product->image) && file_exists($product->image)) {
                        unlink($product->image);
                    }
                    $product_image = $this->imageUpload($request, 'image', 'uploads/product');
                } else {
                    $product_image = $product->image;
                }

            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->image = $product_image;
            $product->save_by = Auth::user()->id;
            $product->ip_address = $request->ip();
            $product->save();
            return redirect()->route('product.index')->with('success', 'Product updated successfully');
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product->image) {
            @unlink($product->image);
        }
        $product->delete();
        return back()->with('success','product deleted successfully');
        
    }
}
