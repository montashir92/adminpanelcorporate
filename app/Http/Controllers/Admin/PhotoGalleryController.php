<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PhotoGallery;
use Illuminate\Http\Request;

class PhotoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photoGallery = PhotoGallery::latest()->get();
        return view('admin.photoGallery.index',compact('photoGallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|max:200',
            'image' => 'required|max:10000||Image|mimes:jpg,png,jpeg,bmp',
          ]);

          $photoGallery = new PhotoGallery();
          $photoGallery->title = $request->title;
          $photoGallery->image = $this->imageUpload($request, 'image', 'uploads/photoGallery');
          $photoGallery->ip_address = $request->ip();
          $photoGallery->save();

          if($photoGallery){
            return back()->with('success','PhotoGallery Added Successfully');
          }
          else{
            return back()->with('error','PhotoGallery Added Fail!');
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PhotoGallery $photoGallery)
    {
        return view('admin.photoGallery.edit',compact('photoGallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'title'=> 'max:255',
            'image' => 'max:1000||Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $slider = PhotoGallery::first();
        $sliderImage = '';
        if ($request->hasFile('image')) {
            if (file_exists($slider->image)) {
                 unlink($slider->image);
            }
            $sliderImage = $this->imageUpload($request, 'image', 'uploads/slider');
        } else {
            $sliderImage = $slider->image;
        }

        $slider->title = $request->title;
        $slider->image = $sliderImage;
        $slider->save();

        return redirect()->route('photo-gallery.index')->with('success','photoGallery Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photoGallery = PhotoGallery::where('id', $id)->first();
        if ($photoGallery->image) {
            @unlink($photoGallery->image);
        }
        $photoGallery->delete();
        return back()->with('success','photoGallery Deleted Successfully');
    }
}
