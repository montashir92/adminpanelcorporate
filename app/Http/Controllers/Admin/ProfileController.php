<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit(){
        $portfolio = Profile::first();
        return view('admin.profile.edit',compact('portfolio'));
    }

    public function update(Request $request){
        $request->validate([
            'name' => 'max:255',
            'description' => 'string',
            'image' => 'max:1000|Image|mimes:jpg,png,jpeg,bmp',
        ]);

        $portfolio = Profile::first();
        $portfolioImage = '';
        if ($request->hasFile('image')) {
            if (!empty($portfolio->image) && file_exists($portfolio->image)) {
                unlink($portfolio->image);
                $portfolioImage = $this->imageUpload($request, 'image', 'uploads/portfolio');
            } else {
                $portfolioImage = $portfolio->image;
            }
            $portfolioImage = $this->imageUpload($request, 'image', 'uploads/portfolio');
        }

        $portfolio->name = $request->name;
        $portfolio->description = $request->description;
        $portfolio->image = $portfolioImage;
        $portfolio->save();

        return back()->with('success','Portfolio Updated Successfully');
    }
}
