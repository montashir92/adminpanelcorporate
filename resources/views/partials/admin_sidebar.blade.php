<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            @php
                $prefix = Request::route()->getPrefix();
                $route = Route::current()->getName();
            @endphp
            <div class="nav">
                {{-- <div class="sb-sidenav-menu-heading">Core</div> --}}
                <a class="nav-link {{ $route == 'dashboard' ? 'active' : '' }}"
                    href="{{ route('dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-door-open"></i></div>
                    Dashboard
                </a>
                <a class="nav-link {{ $route == 'welcome-note.edit' ? 'active' : '' }}"
                    href="{{ route('welcome-note.edit') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-door-open"></i></div>
                    Welcome Note
                </a>
                <a class="nav-link {{ $route == 'slider.index' ? 'active' : '' }}" href="{{ route('slider.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-images"></i></div>
                    Slider Picture
                </a>
                <a class="nav-link {{ $route == 'service.index' ? 'active' : '' }} " href="{{ route('service.index') }}">
                    <div class="sb-nav-link-icon"><i class="fab fa-servicestack"></i></div>
                    Service
                </a>
                <a class="nav-link {{ $prefix == 'product' ? '' : 'collapsed' }}" href="#" data-bs-toggle="collapse"
                    data-bs-target="#collapseLayouts1" aria-expanded="{($prefix == 'product')?'true':'false'}}"
                    aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fab fa-product-hunt"></i></div>
                    Product
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse {{ $prefix == 'product' ? 'collapse show' : '' }}" id="collapseLayouts1"
                    aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">

                        <a class="nav-link {{ $route == 'category.index' ? 'active' : '' }}"
                            href="{{ route('category.index') }}"><i class="fas fa-angle-right"></i>&nbsp;category
                            Entry</a>
                        <a class="nav-link {{ $route == 'product.index' ? 'active' : '' }}"
                            href="{{ route('product.index') }}"><i class="fas fa-angle-right"></i>&nbsp;Product
                            Entry</a>

                    </nav>
                </div>

                <a class="nav-link {{ $route == 'client.index' ? 'active' : '' }}" href="{{ route('client.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-mug-hot"></i></div>
                    Client List
                </a>
                <a class="nav-link {{ $route == 'photo-gallery.index' ? 'active' : '' }}"
                    href="{{ route('photo-gallery.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-image"></i></div>
                    Photo Gallery
                </a>
                <a class="nav-link {{ $route == 'video-gallery.index' ? 'active' : '' }} "
                    href="{{ route('video-gallery.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-video"></i></div>
                    Video Gallery
                </a>
                <a class="nav-link {{ $route == 'content' ? 'active' : '' }} " href="{{ route('content') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-feather"></i></div>
                    Content
                </a>
                <a class="nav-link {{ $route == 'profile' ? 'active' : '' }} " href="{{ route('profile') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                    About Me
                </a>

                <a class="nav-link  " href="{{ route('message') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-sms"></i></div>
                    Message From Website
                </a>
                <a class="nav-link  {{ $route == 'contact.us' ? 'active' : '' }}" href="{{ route('contact.us') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-blender-phone"></i></div>
                    Contact Us.
                </a>
                <a class="nav-link {{ $prefix == 'setting' ? '' : 'collapsed' }} " href="#" data-bs-toggle="collapse"
                    data-bs-target="#collapseLayouts2" aria-expanded="{($prefix =='setting')?'true':'false'}}"
                    aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-wrench"></i></div>
                    Settings
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse {{ $prefix == 'setting' ? 'collapse show' : '' }}" id="collapseLayouts2"
                    aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link {{ $route == 'password.change' ? 'active' : '' }}"
                            href="{{ route('password.change') }}"><i class="fas fa-angle-right"></i>&nbsp; Password
                            Change</a>
                        <a class="nav-link {{ $route == 'user.edit' ? 'active' : '' }}"
                            href="{{ route('user.edit') }}"><i class="fas fa-angle-right"></i>&nbsp; User Update </a>
                    </nav>
                </div>

                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="return confirm('Are you sure logout from Admin Panel')">
                    <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
                    Log Out
                </a>
            </div>
        </div>
    </nav>
</div>
