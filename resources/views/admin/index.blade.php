@extends('layouts.admin')
@section('title', 'Dashboard')
@section('admin-content')
    <main class="">
        <div class="container-fluid">
            <div class="heading-title p-2">
                <span class="my-3 heading "><i class="fas fa-home"></i> <a class="" href="">Home</a> >
                    Dashboard</span>
            </div>
            <div class="row mt-3">
                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('slider.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-images fa-2x text-white"></i> <span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Slider</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('service.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fab fa-servicestack text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Service</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('product.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fab fa-product-hunt text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Product</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('client.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-mug-hot text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Client</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('photo-gallery.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-image text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Photo Gallery</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('photo-gallery.index') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-video text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Video Gallery</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('message') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-sms text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">Message</p>
                        </a>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 ">
                    <div class="card mb-3 dashboard-card " style="background: linear-gradient(#e66465, #9198e5)">
                        <a href="{{ route('profile') }}" class="card-body mx-auto text-decoration-none">
                            <div class=" d-flex justify-content-center align-items-center">
                                <i class="fas fa-user text-white fa-2x"></i><span class="count"></span>
                            </div>

                            <p class="dashboard-card-text text-white ">About Me</p>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
