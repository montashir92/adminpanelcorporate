@extends('layouts.admin')
@section('title', 'Edit Content')
@section('admin-content')
    <main>
        <div class="container ">
            <div class="heading-title p-2 my-2">
                <span class="my-3 heading "><i class="fas fa-home"></i> <a class=""
                        href="{{ route('dashboard') }}">Home</a> Edit Content</span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex">
                            <div class="table-head"><i class="fas fa-table me-1"></i>Edit Content <a href=""
                                    class="float-right"></a></div>
                            <a href="{{ route('content.active') }}" class="btn btn-success btn-sm ms-auto">Content
                                @if ($content->status == true) Deactive @else Active @endif</a>
                        </div>
                        @if ($content->status == true)
                            <div class="card-body table-card-body">
                                <form method="post" action="{{ route('content.update') }}" enctype="multipart/form-data">

                                    @csrf
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong><label>Title</label> <span class="float-right">:</span></strong>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" id="title"
                                                class="form-control my-form-control @error('title') is-invalid @enderror"
                                                name="title" value="{{ $content->title }}">
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-3">
                                            <strong><label>Description</label> <span
                                                    class="float-right">:</span></strong>
                                        </div>
                                        <div class="col-md-9 mt-2">
                                            <textarea class="form-control  @error('description') is-invalid @enderror"
                                                name="description" id="details">{{ $content->description }}</textarea>
                                            @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-3">
                                            <strong><label>Image</label> <span class="float-right">:</span></strong>
                                        </div>
                                        <div class="col-md-5 mt-2">
                                            <input type="file" class="form-control my-form-control" id="image" name="image"
                                                onchange="readURL(this);">
                                        </div>
                                        <div class="col-md-4 mt-2">
                                            <img class="form-controlo img-thumbnail" src="#" id="previewImage"
                                                style="height:100px;width:120px; background: #3f4a49;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary btn-sm mt-2 float-right  mt-3 "
                                                value="Submit">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
@push('admin-js')
    <script src="{{ asset('admin/js/ckeditor.js') }}"></script>
    <script>

    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#details'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                        .width(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "{{isset($content->image)? asset($content->image): asset('noimage.png') }}";
    </script>
    <script>

    </script>
@endpush
