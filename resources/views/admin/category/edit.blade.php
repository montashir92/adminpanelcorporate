@extends('layouts.admin')
@section('title', 'Edit Category')
@section('admin-content')
    <main>
        <div class="container ">
            <div class="heading-title p-2 my-2">
                <span class="my-3 heading "><i class="fas fa-home"></i> <a class=""
                        href="{{ route('dashboard') }}">Home</a> > Category Update</span>
            </div>
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-cogs"></i>
                    Update Cateogry
                </div>
                <div class="card-body table-card-body p-3 mytable-body">
                    <form action="{{ route('category.update', $category->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Category Name</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="name" value="{{ $category->name }}"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary float-right" value="Submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </main>
@endsection
@push('admin-js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "{{ asset($category->image) }}";
    </script>

@endpush
