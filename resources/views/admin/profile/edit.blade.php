@extends('layouts.admin')
@section('title', 'Edit Profile')
@section('admin-content')
    @push('admin-css')
        {{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}

    @endpush
    <main>
        <div class="container ">
            <div class="heading-title p-2 my-2">
                <span class="my-3 heading "><i class="fas fa-home"></i> <a class=""
                        href="{{ route('dashboard') }}">Home</a> Edit Profile</span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class=""><i class="fas fa-user me-1"></i>Edit Profile</div>
                        </div>
                        <div class="card-body table-card-body">
                            <form method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <strong><label>Name</label> <span class="float-right">:</span></strong>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="title"
                                            class="form-control my-form-control @error('name') is-invalid @enderror"
                                            name="name" value="{{ $portfolio->name }}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-3">
                                        <strong><label>Description</label> <span class="float-right">:</span></strong>
                                    </div>
                                    <div class="col-md-9 mt-2">
                                        <textarea class="form-control  @error('description') is-invalid @enderror"
                                            name="description" id="description"
                                            rows="10">{{ $portfolio->description }}</textarea>
                                        @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <strong><label>Image</label> <span class="float-right">:</span></strong>
                                    </div>
                                    <div class="col-md-5 mt-2">
                                        <input type="file" class="form-control my-form-control" id="image" name="image"
                                            onchange="readURL(this);">
                                    </div>
                                    <div class="col-md-4 mt-2">
                                        <img class="form-controlo img-thumbnail" src="#" id="previewImage"
                                            style="height:100px;width:120px; background: #3f4a49;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-sm mt-2 float-right  mt-3 "
                                            value="Submit">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
@push('admin-js')
    <script src="{{ asset('admin/js/ckeditor.js') }}"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                        .width(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "{{isset($portfolio->image)? asset($portfolio->image): asset('noimage.png') }}";
     
    </script>
@endpush
