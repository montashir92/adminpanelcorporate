@extends('layouts.admin')
@section('title', 'Photo-gallery Add')
@section('admin-content')
    <main>
        <div class="container ">
            <div class="heading-title p-2 my-2">
                <span class="my-3 heading "><i class="fas fa-home"></i> <a class=""
                        href="{{ route('dashboard') }}">Home</a> >Video Gallery</span>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="{{ route('video-gallery.update', $videoGallery->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card">
                            <div class="card-header">
                                <div class=""><i class="fas fa-cogs me-1"></i>Edit Video Gallery</div>
                            </div>
                            <div class="card-body table-card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label><strong>Title :</strong></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" value="{{ $videoGallery->title }}"
                                            class="form-control my-form-control" name="title">
                                    </div>
                                    <div class="col-md-3">
                                        <label><strong>Video Link :</strong></label>
                                    </div>
                                    <div class="col-md-9 mt-2">
                                        <input type="url" value="{{ $videoGallery->video_link }}" name="video_link"
                                            class="form-control my-form-control">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-sm mt-3 float-right"
                                            value="Submit">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </main>
@endsection
@push('admin-js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                        .width(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "/noimage.png";
    </script>


@endpush
