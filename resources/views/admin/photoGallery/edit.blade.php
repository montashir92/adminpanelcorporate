@extends('layouts.admin')
@section('title', 'Slider Add')
@section('admin-content')
    <main>
        <div class="container ">
            <div class="heading-title p-2 my-2 d-flex">
                <span class=" heading "><i class="fas fa-home"></i> <a class=""
                        href="{{ route('dashboard') }}">Home</a> >Photo Gallery</span>
                <span class=" heading ms-auto"><a class="" href="{{ route('photo-gallery.index') }}">Back</a>
                    >Photo Gallery</span>

            </div>
            <form action="{{ route('photo-gallery.update', $photoGallery->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class=""><i class="fas fa-cogs me-1"></i>Update Photo Gallery</div>
                            </div>
                            <div class="card-body table-card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label><strong>Title :</strong></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" value="{{ $photoGallery->title }}"
                                            class="form-control my-form-control" name="title">
                                    </div>
                                    <div class="col-md-3">
                                        <label><strong>Image :</strong></label>
                                    </div>
                                    <div class="col-md-5 mt-2">
                                        <input type="file" class="form-control my-form-control" id="image" name="image"
                                            onchange="readURL(this);">
                                    </div>
                                    <div class="col-md-4 mt-2">
                                        <img class="form-controlo img-thumbnail" src="#" id="previewImage"
                                            style="height:100px;width:120px; background: #3f4a49;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-sm mt-2 mt-3 float-right"
                                            value="Submit">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </main>
@endsection
@push('admin-js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                        .width(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "/noimage.png";
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewImage')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        document.getElementById("previewImage").src = "{{ asset($photoGallery->image) }}";
    </script>
@endpush
